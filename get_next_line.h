/*
** get_next_line.h for get_next_line in /home/mariet_v//celem/gnl
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Nov 19 11:17:58 2012 valentin mariette
** Last update Fri Mar 15 16:57:35 2013 valentin mariette
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define MAX_LEN_BUFFER 4096
# define MAX_LEN_READ 4096

void	get_new_buffer(char *ptr, int i);
char	*case_big_enough(char *string, char *buffer);
char	*case_too_small(char *buf, int fd, int len, int r_v);
char	*get_next_line(const int fd);

#endif
