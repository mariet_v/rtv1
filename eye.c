/*
** eye.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Feb 26 10:07:04 2013 valentin mariette
** Last update Sun Mar 17 13:04:16 2013 valentin mariette
*/

#include <stdio.h>
#include "rtv1.h"
#include "draw_line.h"

void	moove_eye(t_pos *coord, t_droite *vect)
{
  vect->x += coord->eye->x;
  vect->z += coord->eye->z;
  vect->y += coord->eye->y;
}

t_eye	*trans_rot(t_eye *eye, t_obj *obj, t_droite *vect, t_droite *tmp)
{
  t_eye	*eye_tmp;

  if (!(eye_tmp = malloc(sizeof(t_eye))))
    error(2);
  eye_tmp->x = eye->x - obj->x;
  eye_tmp->y = eye->y - obj->y;
  eye_tmp->z = eye->z + obj->z;
  rot_x(eye_tmp, obj->rot_x);
  rot_y(eye_tmp, obj->rot_y);
  rot_z(eye_tmp, obj->rot_z);
  tmp->x = vect->x;
  tmp->y = vect->y;
  tmp->z = vect->z;
  if (obj->rot_x > 0)
    rot_vectx(vect, tmp, 360 - obj->rot_x);
  if (obj->rot_y > 0)
    rot_vecty(vect, tmp, 360 - obj->rot_y);
  if (obj->rot_z > 0)
    rot_vectz(vect, tmp, 360 - obj->rot_z);
  return (eye_tmp);
}
