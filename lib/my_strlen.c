/*
** exo3.c for my_strlen in /home/mariet_v//day4
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Oct  4 10:19:22 2012 valentin mariette
** Last update Fri Mar 15 17:51:55 2013 valentin mariette
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    i++;
  return (i);
}
