/*
** exo2.c for my_putstr in /home/mariet_v//day4
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Oct  4 10:19:22 2012 valentin mariette
** Last update Wed Dec 19 15:02:29 2012 valentin mariette
*/

int	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i++;
    }
}
