/*
** wolf3D.h for wolf3D in /home/mariet_v//igraph/wolf3D
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Jan  7 15:56:12 2013 valentin mariette
** Last update Sun Mar 17 14:45:41 2013 valentin mariette
*/

#ifndef RTV1_H_
# define RTV1_H_
# include <mlx.h>
# include <mlx_int.h>
# include "draw_line.h"
# include "map.h"

typedef struct	s_coord
{
  double	posX;
  double	posY;
  double	dirX;
  double	dirY;
}		t_coord;

typedef struct	s_alldata
{
  t_img		*img;
  char		*data;
  void		*mlx_ptr;
  void		*win_ptr;
}		t_alldata;

/*
** signaux
*/

int	key_manager(int keycode, t_alldata *foobar);
int	mouse_manager(int button, int x, int y, void *param);
int	expose_manager(t_alldata *foobar);

/*
**programme
*/

void	put_in_struct(t_alldata *foobar);
char	*get_data(t_img *img);
void	make_img(char *data);
int	debin(int color, int i);
int	calc(t_pos *coord, t_obj *map, double k1, int color);
t_pos	*init_coord();
int	get_bpp(int bpp);
void	get_new_buffer(char *ptr, int i);
char	*case_big_enough(char *string, char *buffer);
char	*case_too_small(char *buf, int fd, int len, int r_v);
char	*get_next_line(const int fd);
void	moove_eye(t_pos *coord, t_droite *vect);
double	inter_sphere(t_eye *eye, t_droite *vect, double r);
double	inter_plan(t_eye *eye, t_droite *vect);
double	inter_cylindre(t_eye *eye, t_droite *vect, double r);
void	find_good_color(double *k1, double k2, int *color, t_obj *tmp);
t_eye	*trans_rot(t_eye *eye, t_obj *obj, t_droite *vect, t_droite *tmp);
void	rot_x(t_eye *eye, int rot);
void	rot_y(t_eye *eye, int rot);
void	rot_z(t_eye *eye, int rot);
void	rot_vectx(t_droite *vect, t_droite *vect_tmp, int rot);
void	rot_vecty(t_droite *vect, t_droite *vect_tmp, int rot);
void	rot_vectz(t_droite *vect, t_droite *vect_tmp, int rot);

#endif
