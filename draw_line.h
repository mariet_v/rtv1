/*
** draw_line.h for draw_line in /home/mariet_v//igraph/draw_line
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Nov 23 15:38:22 2012 valentin mariette
** Last update Sun Mar 17 13:02:54 2013 valentin mariette
*/

#ifndef DRAW_LINE_H_
# define DRAW_LINE_H_

typedef struct	s_eye
{
  float		x;
  float		y;
  float		z;
}		t_eye;

typedef struct	s_position
{
  int		x;
  int		y;
  int		x2;
  int		y2;
  int		x_max;
  int		y_max;
  int		color;
  t_eye		*eye;
}		t_pos;

typedef struct s_droite
{
  double	x;
  double	y;
  double	z;
  double	k;
}		t_droite;

int	pix_to_img(char *data, int x, int y, t_pos *coord);
int	cpix_to_img(char *data, t_pos *coord);
int	draw_cas_un(char *data, t_pos *coord);
int	draw_cas_cinq(char *data, t_pos *coord);
int	abs(int a);
t_pos	*invert_ab(t_pos *coord);
int	draw_line(char *data, t_pos *coord);

#endif
