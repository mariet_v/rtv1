/*
** make_img.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Feb  8 10:18:23 2013 valentin mariette
** Last update Thu Mar 26 11:44:52 2015 Lazio
*/

#include <math.h>
#include <stdio.h>
#include "rtv1.h"
#include "draw_line.h"
#include "map.h"

void	make_img(char *data)
{
  t_pos	*coord;
  t_obj	*map;

  coord = init_coord();
  if (!(map = get_map()))
      error(1);
  while (coord->y < coord->y_max)
    {
      coord->x = 0;
      while (coord->x < coord->x_max)
	{
	  coord->color = calc(coord, map, -1, 0);
	  cpix_to_img(data, coord);
	  coord->x++;
	}
      coord->y++;
    }
  free(coord->eye);
  free(coord);
  free_map(map);
}

int		calc(t_pos *coord, t_obj *obj_list, double k1, int color)
{
  t_droite	vect;
  t_droite	vect_tmp;
  t_eye		*eye_tmp;
  double	k2;
  t_obj		*tmp;

  vect.x = 100;
  vect.y = coord->x_max / 2 - coord->x;
  vect.z = coord->y_max / 2 - coord->y;
  moove_eye(coord, &vect);
  tmp = obj_list;
  while (tmp != NULL)
    {
      eye_tmp = trans_rot(coord->eye, tmp, &vect, &vect_tmp);
      if (tmp->type == 1)
	k2 = inter_sphere(eye_tmp, &vect_tmp, tmp->size);
      else if (tmp->type == 2)
	k2 = inter_plan(eye_tmp, &vect_tmp);
      else if (tmp->type == 3)
	k2 = inter_cylindre(eye_tmp, &vect_tmp, tmp->size);
      find_good_color(&k1, k2, &color, tmp);
      tmp = tmp->next;
      free(eye_tmp);
    }
  return (color);
}

void	find_good_color(double *k1, double k2, int *color, t_obj *tmp)
{
  if (*k1 == -1 && k2 > 0)
    {
      *k1 = k2;
      *color = tmp->color;
    }
  else if (*k1 > 0 && k2 > 0 && k2 < *k1)
    {
      *k1 = k2;
      *color = tmp->color;
    }
}
