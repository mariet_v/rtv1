/*
** map.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Feb 14 10:44:46 2013 valentin mariette
** Last update Sun Mar 17 19:38:21 2013 valentin mariette
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "rtv1.h"
#include "map.h"

t_obj	*get_map(void)
{
  int	file;
  char	*line;
  t_obj	*map;

  map = NULL;
  if ((file = open("map", O_RDONLY)) <= 0)
    error(1);
  line = get_next_line(file);
  while (line != NULL)
    {
      map = add_object(line, map, 0);
      line = get_next_line(file);
    }
  close(file);
  return (map);
}

t_obj	*add_object(char *line, t_obj *map, int i)
{
  t_obj	*new;
  t_obj	*tmp;

  if (!(new = malloc(sizeof(t_obj))))
    error(2);
  new->type = my_getnbr(&line[0]);
  new->x = my_getnbr(&line[next(line, &i)]);
  new->y = my_getnbr(&line[next(line, &i)]);
  new->z = my_getnbr(&line[next(line, &i)]);
  new->rot_x = my_getnbr(&line[next(line, &i)]);
  new->rot_y = my_getnbr(&line[next(line, &i)]);
  new->rot_z = my_getnbr(&line[next(line, &i)]);
  new->size = my_getnbr(&line[next(line, &i)]);
  new->color = my_getnbr(&line[next(line, &i)]);
  new->next = NULL;
  if (map == NULL)
    return (new);
  else
    {
      tmp = map;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new;
      return (map);
    }
}

int	next(char *line, int *i)
{
  if (line[*i + 1] != '\0')
    *i = *i + 1;
  while (line[*i] != ' ' && line[*i] != '\t' && line[*i] != '\0')
    *i = *i + 1;
  while ((line[*i] == ' ' || line[*i] == '\t') && line[*i] != '\0')
    *i = *i + 1;
  return (*i);
}

void	free_map(t_obj	*map)
{
  t_obj	*tmp;
  t_obj	*tmp2;

  tmp = map;
  while (tmp2 != NULL)
    {
      tmp2 = tmp->next;
      free(tmp);
      tmp = tmp2;
    }
}
