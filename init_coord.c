/*
** init_coord.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Feb 14 10:07:46 2013 valentin mariette
** Last update Sun Mar 17 12:53:14 2013 valentin mariette
*/

#include <stdlib.h>
#include "rtv1.h"

t_pos		*init_coord()
{
  t_pos		*coord;
  t_eye		*eye;

  eye = malloc(sizeof(*eye));
  coord = malloc(sizeof(*coord));
  if (eye == NULL || coord == NULL)
    error(2);
  eye->x = -300;
  eye->y = 0;
  eye->z = 10;
  coord->x = 0;
  coord->y = 0;
  coord->x_max = 800;
  coord->y_max = 600;
  coord->eye = eye;
  return (coord);
}
