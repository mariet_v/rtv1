/*
** rotate.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Mar 14 11:50:39 2013 valentin mariette
** Last update Sun Mar 17 13:13:27 2013 valentin mariette
*/

#include <math.h>
#include "rtv1.h"

void		rot_x(t_eye *eye, int rot)
{
  double	y2;
  double	z2;
  double	rot_rad;

  rot_rad = 3.141592654 * rot / 180;
  y2 = eye->y * cos(rot_rad) - eye->z * sin(rot_rad);
  z2 = eye->y * sin(rot_rad) + eye->z * cos(rot_rad);
  eye->y = y2;
  eye->z = z2;
}

void		rot_y(t_eye *eye, int rot)
{
  double	x2;
  double	z2;
  double	rot_rad;

  rot_rad = 3.141592654 * rot / 180;
  x2 = eye->x * cos(rot_rad) + eye->z * sin(rot_rad);
  z2 = - eye->x * sin(rot_rad) + eye->z * cos(rot_rad);
  eye->x = x2;
  eye->z = z2;
}

void		rot_z(t_eye *eye, int rot)
{
  double	x2;
  double	y2;
  double	rot_rad;

  rot_rad = 3.141592654 * rot / 180;
  x2 = eye->x * cos(rot_rad) + eye->y * sin(rot_rad);
  y2 = - eye->x * sin(rot_rad) + eye->y * cos(rot_rad);
  eye->x = x2;
  eye->y = y2;
}
