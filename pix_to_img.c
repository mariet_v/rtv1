/*
** main.c for tp1_windows_empty in /home/mariet_v//igraph/tp1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Nov  8 09:41:02 2012 valentin mariette
** Last update Thu Mar 26 11:52:43 2015 Lazio
*/

#include <stdio.h>
#include "draw_line.h"
#include "rtv1.h"

int	pix_to_img(char *data, int x, int y, t_pos *coord)
{
  int	z;
  int	i;
  int	nbr;

  if (x > coord->x_max || x <= 0 || y <= 0 || y > coord->y_max)
    return;
  nbr = coord->color;
  i = 0;
  z = (y - 1) * coord->x_max * 32 / 8 + (x - 1) * 4;
  while (i < 4)
    {
      data[z + i++] = nbr;
    }
}

int	cpix_to_img(char *data, t_pos *coord)
{
  int	z;
  int	i;
  unsigned char *foo;
  foo = (unsigned char *) data;
  if (coord->x > coord->x_max || coord->x <= 0 || coord->y <= 0 ||
      coord->y > coord->y_max)
    return;
  i = 0;
  z = (coord->y - 1) * coord->x_max * get_bpp(0) / 8 + (coord->x - 1) * 4;
  while (i < 3)
    data[z + i] = debin(coord->color, i++);
}
  
int	debin(int color, int i)
{
  int	tab[3];
  tab[2] = color >> 16;
  tab[1] = (color >> 8) - ((color >> 16) << 8);
  tab[0] = color - ((color >> 8) << 8);
  return (tab[i]);
}

int		get_bpp(int bpp)
{
  static int	s_bpp;

  if (bpp != 0)
    s_bpp = bpp;
  else
    return (s_bpp);
  return (0);
}
