/*
** main.c for wolf3D in /home/mariet_v//igraph/wolf3D
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Mon Jan  7 15:50:35 2013 valentin mariette
** Last update Sun Mar 17 13:08:40 2013 valentin mariette
*/

#include <stdlib.h>
#include <X11/X.h>
#include "draw_line.h"
#include "rtv1.h"

int		main(int argc, char **argv)
{
  int		(*key_man)();
  int		(*mouse_man)();
  int		(*expose_man)();
  t_alldata	*window;
  t_coord	*coord;

  window = malloc(sizeof(*window));
  if (window == NULL)
    error(2);
  key_man = &key_manager;
  mouse_man = &mouse_manager;
  expose_man = &expose_manager;
  put_in_struct(window);
  my_putstr("calculating done\n");
  mlx_key_hook(window->win_ptr, key_manager, window);
  mlx_mouse_hook(window->win_ptr, mouse_manager, 0);
  mlx_expose_hook(window->win_ptr, expose_manager, window);
  mlx_loop(window->mlx_ptr);
  return (0);
}

void	put_in_struct(t_alldata *foobar)
{
  char	*data;
  void	*mlx_ptr;
  void	*win_ptr;
  t_img	*img;

  foobar->mlx_ptr = mlx_init();
  if (foobar->mlx_ptr == NULL)
    error(3);
  foobar->win_ptr = mlx_new_window(foobar->mlx_ptr, 800, 600, "rtv1");
  if (foobar->win_ptr == NULL)
    error(3);
  foobar->img = mlx_new_image(foobar->mlx_ptr, 800, 600);
  if (foobar->img == NULL)
    error(3);
  foobar->data = get_data(foobar->img);
  make_img(foobar->data);
  mlx_put_image_to_window(foobar->mlx_ptr, foobar->win_ptr, foobar->img, 0, 0);
}

char	*get_data(t_img *img)
{
  char	*data;
  int	bpp;
  int	sizeline;
  int	endian;

  data = mlx_get_data_addr(img, &bpp, &sizeline, &endian);
  if (data == NULL)
    error(3);
  get_bpp(bpp);
  return (data);
}
