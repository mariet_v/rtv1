/*
** object.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Tue Feb 26 11:37:07 2013 valentin mariette
** Last update Sun Mar 17 14:44:15 2013 valentin mariette
*/

#include <stdio.h>
#include <math.h>
#include "rtv1.h"
#include "draw_line.h"

double		inter_sphere(t_eye *eye, t_droite *vect, double r)
{
  double	delta;
  double	a;
  double	b;
  double	c;
  double	k;
  double	k2;

  a = pow(vect->x, 2) + pow(vect->y, 2) + pow(vect->z, 2);
  b = 2 * (eye->x * vect->x + eye->y * vect->y + eye->z * vect->z);
  c = pow(eye->x, 2) + pow(eye->y, 2) + pow(eye->z, 2) - pow(r, 2);
  delta = pow(b, 2) - 4 * a * c;
  if (delta > 0)
    {
      k = -(- b + sqrt(delta)) / (2 * a);
      k2 = -(- b - sqrt(delta)) / (2 * a);
      if (k > 0 && k < k2)
	return (k);
      else if (k2 > 0 && k2 < k)
	return (k2);
      else
	return (0);
    }
  else
    return (0);
}

double		inter_plan(t_eye *eye, t_droite *vect)
{
  double	z;

  z = 0;
  if (vect->z > -0.0001 && vect->z < 0.0001)
    return (0);
  else
    return (- eye->z / vect->z);
}

double		inter_cylindre(t_eye *eye, t_droite *vect, double r)
{
  double        delta;
  double        a;
  double        b;
  double        c;
  double        k;
  double        k2;

  a = pow(vect->x, 2) + pow(vect->y, 2);
  b = 2 * (eye->x * vect->x + eye->y * vect->y);
  c = pow(eye->x, 2) + pow(eye->y, 2) - pow(r, 2);
  delta = pow(b, 2) - 4 * a * c;
  if (delta > 0)
    {
      k = -(- b + sqrt(delta)) / (2 * a);
      k2 = -(- b - sqrt(delta)) / (2 * a);
      if (k > 0 && k < k2)
        return (k);
      else if (k2 > 0 && k2 < k)
        return (k2);
      else
        return (0);
    }
  else
    return (0);
}
