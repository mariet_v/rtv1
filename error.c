/*
** erreur.c for fdf in /home/mariet_v//igraph/fdf
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Dec  6 17:56:41 2012 valentin mariette
** Last update Sun Mar 17 13:03:09 2013 valentin mariette
*/

#include <stdlib.h>
#include "rtv1.h"

void	error(int flag)
{
  if (flag == 1)
    my_putstr("Erreur de lecture de la map\n");
  else if (flag == 2)
    my_putstr("Erreur d'allocation mémoire.\n");
  else if (flag == 3)
    my_putstr("Erreur interne à la mlx.\n");
  if (flag == 4)
    my_putstr("Fichier non valide.\n");
  exit(-1);
}
