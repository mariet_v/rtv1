/*
** rotate_vect.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Mar 14 12:39:57 2013 valentin mariette
** Last update Sun Mar 17 13:14:04 2013 valentin mariette
*/

#include <math.h>
#include "rtv1.h"

void		rot_vectx(t_droite *vect, t_droite *vect_tmp, int rot)
{
  double	rot_rad;

  rot_rad = 3.141592654 * rot / 180;
  vect_tmp->y = vect->y * cos(rot_rad) - vect->z * sin(rot_rad);
  vect_tmp->z = vect->y * sin(rot_rad) + vect->z * cos(rot_rad);
}

void		rot_vecty(t_droite *vect, t_droite *vect_tmp, int rot)
{
  double	rot_rad;

  rot_rad = 3.141592654 * rot / 180;
  vect_tmp->x = vect->x * cos(rot_rad) + vect->z * sin(rot_rad);
  vect_tmp->z = - vect->x * sin(rot_rad) + vect->z * cos(rot_rad);
}

void		rot_vectz(t_droite *vect, t_droite *vect_tmp, int rot)
{
  double	rot_rad;

  rot_rad = 3.141592654 * rot / 180;
  vect_tmp->x = vect->x * cos(rot_rad) + vect->y * sin(rot_rad);
  vect_tmp->y = - vect->x * sin(rot_rad) + vect->y * cos(rot_rad);
}
