/*
** draw_line.c for draw_line in /home/mariet_v//igraph/draw_line
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Nov 23 15:29:36 2012 valentin mariette
** Last update Sun Mar 17 13:17:29 2013 valentin mariette
*/

#include <stdio.h>
#include "draw_line.h"

int	abs(int a)
{
  if (a < 0)
    return (-a);
  else
    return (a);
}

t_pos	*invert_ab(t_pos *coord)
{
  int	tmp;

  tmp = coord->x;
  coord->x = coord->x2;
  coord->x2 = tmp;
  tmp = coord->y;
  coord->y = coord->y2;
  coord->y2 = tmp;
}

int	draw_line(char *data, t_pos *coord)
{
  if (coord->x <= coord->x2 &&
      (coord->x2 - coord->x) >= abs(coord->y2 - coord->y))
    draw_cas_un(data, coord);
  else if ((coord->x >= coord->x2) &&
	   (coord->x - coord->x2) >= abs(coord->y2 - coord->y))
    draw_cas_un(data, invert_ab(coord));
  else if (coord->y2 <= coord->y &&
	   (coord->x2 - coord->x) <= abs(coord->y2 - coord->y))
    draw_cas_cinq(data, coord);
  else if ((coord->y <= coord->y2) &&
	   (coord->x - coord->x2) <= abs(coord->y2 - coord->y))
    draw_cas_cinq(data, invert_ab(coord));
}

int	draw_cas_un(char *data, t_pos *coord)
{
  int	x;

  x = coord->x;
  if (coord->x2 - coord->x == 0)
    return;
  while (x <= coord->x2)
    {
      pix_to_img(data, x, coord->y
		 + ((coord->y2 - coord->y) *
		    (x - coord->x)) / (coord->x2 - coord->x), coord);
      x++;
    }
}

int	draw_cas_cinq(char *data, t_pos *coord)
{
  int	y;

  y = coord->y2;
  if (coord->y2 - coord->y == 0)
    return;
  while (y <= coord->y)
    {
      pix_to_img(data, coord->x - ((coord->x2 - coord->x) * (y - coord->y))
		 / abs(coord->y2 - coord->y), y, coord);
      y++;
    }
}
