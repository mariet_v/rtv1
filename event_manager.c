/*
** event_manager.c for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Fri Feb  8 10:12:51 2013 valentin mariette
** Last update Sun Mar 17 13:03:50 2013 valentin mariette
*/

#include "rtv1.h"

int	key_manager(int keycode, t_alldata *foobar)
{
  if (keycode == 65307)
    {
      free(foobar);
      exit(0);
    }
  mlx_put_image_to_window(foobar->mlx_ptr, foobar->win_ptr, foobar->img, 0, 0);
}

int	mouse_manager(int button, int x, int y, void *param)
{
}

int	expose_manager(t_alldata *foobar)
{
  mlx_put_image_to_window(foobar->mlx_ptr, foobar->win_ptr, foobar->img, 0, 0);
}
