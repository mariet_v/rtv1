##
## Makefile for fdf in /home/mariet_v//igraph/fdf
## 
## Made by valentin mariette
## Login   <mariet_v@epitech.net>
## 
## Started on  Mon Dec  3 16:20:31 2012 valentin mariette
## Last update Thu Mar 14 14:47:25 2013 valentin mariette
##

SRC	= main.c \
	error.c \
	event_manager.c \
	draw_line.c \
	pix_to_img.c \
	make_img.c \
	init_coord.c \
	get_next_line.c \
	map.c \
	eye.c \
	object.c \
	rotate.c \
	rotate_vect.c \
	lib/my_putstr.c \
	lib/my_putchar.c \
	lib/my_getnbr.c \
	lib/my_strlen.c \
	lib/my_strnndup.c 

OBJ	= $(SRC:.c=.o)

NAME	= rtv1

$(NAME):  $(OBJ)
	cc -o $(NAME) $(OBJ) -lm -lmlx -lXext -lX11

all:    $(NAME)

clean:
	rm -f $(OBJ)

fclean:  clean
	rm -rf $(NAME)
re : fclean $(NAME)

reclean: re
	rm -f $(OBJ)