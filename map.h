/*
** map.h for rtv1 in /home/mariet_v//igraph/rtv1
** 
** Made by valentin mariette
** Login   <mariet_v@epitech.net>
** 
** Started on  Thu Feb 14 10:46:25 2013 valentin mariette
** Last update Sun Mar 17 14:51:12 2013 valentin mariette
*/

#ifndef MAP_H_
# define MAP_H_

typedef struct	s_obj
{
  int		type;
  double	x;
  double	y;
  double	z;
  int		rot_x;
  int		rot_y;
  int		rot_z;
  double	size;
  int		color;
  struct s_obj	*next;
}		t_obj;

t_obj	*get_map(void);
t_obj	*get_liste_object(void);
t_obj	*add_object(char *line, t_obj *map, int i);
int	next(char *line, int *i);
void	free_map(t_obj  *map);

#endif
